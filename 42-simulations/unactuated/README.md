# Phasma steady state investigation
## Setup
* Control profile : Passive, ideal pointing, passive 
* Attitude commands : 
```text
0.0 SC[0] FswTag = PASSIVE_FSW
400.0 SC[0] FswTag = PROTOTYPE_FSW
400.0 SC[0] Cmd Angles = [0.0 40.0 0.0] deg, Seq = 123 wrt L Frame
600.0 SC[0] FswTag = PASSIVE_FSW
```
* Initial conditions : 
	* Attitude : `[0 0 0]`  1-2-3 Euler angles. 
	* Angular velocity : Zero angular rates. This is practically unachievable, but the angular velocity is going to be eliminated by the ideal pointing command, so it does not matter.
* Satellite model : Simplified obj model of PHASMA v2 (2 deployable antennas)
* Ground stations : Add a SatNOGS station in Greece
* Sensors : Model only the FoV of the FSS on the -Z face. The sensor will not be used at this simulation, but we need to experiment with the FoV definition workflow.

*To set the geometry of the satellite, one must place the provided `Phasma_v2.obj` in the Model directory of the 42 installation directory.*

## Goals - expected results
- [x] Verify correct orbit through 42 GUI
- [x] Verify correct positioning of the SatNOGS station through 42 GUI
- [x] Verify correct interpretation (though visualization) of the .obj file
> Completed with an exception : Did not try to assign colors to the various surfaces
- [x] Verify that the FoV cones are set correctly 
- [x] Investigate the steady state attitude (if any) of the satellite
