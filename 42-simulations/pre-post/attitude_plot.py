import matplotlib.pyplot as plt 
import pandas as pd
import numpy as np

# simulation input-output file directory
simul_dir = "42-simulations/unactuated/"

# read the timestep from the Inp_sim file
inp_sim_path = simul_dir + "Inp_Sim.txt"
with open(inp_sim_path,'r') as input_file: 
    linenum = 0     
    # File output interval information is located in line 5
    for line in input_file : 
        if linenum < 4 : 
            pass
        elif linenum == 4 : 
            timestep = line.split()
            timestep = float(timestep[0])
        else :
            break
        linenum += 1 

# Define colors for plotting 
colors = ["dodgerblue","forestgreen","firebrick",'darkmagenta']

# ================== Plot Euler angles in the L frame =======================
# The L frame is the Local-Horizontal Local-Vertical (LHLV) frame

eulang_path = simul_dir + "RPY.42"
colnames = ["Roll","Pitch","Yaw"]
RPY = pd.read_csv(eulang_path,delimiter = ' ',engine = "pyarrow",header=None,names = colnames)

# construct time array
datasize = RPY.shape[0]
times = np.zeros(datasize)
for i in range(1,datasize):
    times[i] = times[i-1] + timestep


fig1,ax1 = plt.subplots()
l1, = ax1.plot(times,RPY['Roll'], label = "Roll",c= colors[0])
l2, = ax1.plot(times,RPY['Pitch'], label = "Pitch",c= colors[1])
l3, = ax1.plot(times,RPY['Yaw'], label = "Yaw",c= colors[2])
handles = [l1,l2,l3]

ax1.grid()
ax1.set_title("Euler angles in the LHLV frame")
ax1.set_ylabel("Angles [deg]")
ax1.set_xlabel("Time [sec]")
ax1.legend(handles = handles)


# ==================== Plot Quaternions in the N frame ====================
quat_path = simul_dir + "qbn.42"
colnames = ["q1","q2","q3","q0"]
quat = pd.read_csv(quat_path,delimiter = ' ',engine = "pyarrow",header=None,names = colnames)

# fig2,ax2 = plt.subplots()
# l1, = ax2.plot(times,quat['q1'], label = "q1",c= colors[0])
# l2, = ax2.plot(times,quat['q2'], label = "q2",c= colors[1])
# l3, = ax2.plot(times,quat['q3'], label = "q3",c= colors[2])
# l4, = ax2.plot(times,quat['q0'], label = "q0-scalar",c= colors[3])
# handles = [l1,l2,l3,l4]

# ax2.grid()
# ax2.set_title("Quaternions in the N frame")
# ax2.set_ylabel("Quaternion")
# ax2.set_xlabel("Time [sec]")
# ax2.legend(handles = handles)


# ================== Plot angular velocity in the N frame =======================
# The L frame is the Local-Horizontal Local-Vertical (LHLV) frame

omega_path = simul_dir + "wbn.42"
colnames = ["omega_x","omega_y","omega_z"]
omega = pd.read_csv(omega_path,delimiter = ' ',engine = "pyarrow",header=None,names = colnames)

fig3,ax3 = plt.subplots()
l1, = ax3.plot(times,omega['omega_x'], label = "omega_x",c= colors[0])
l2, = ax3.plot(times,omega['omega_y'], label = "omega_y",c= colors[1])
l3, = ax3.plot(times,omega['omega_z'], label = "omega_z",c= colors[2])
handles = [l1,l2,l3]

ax3.grid()
ax3.set_title("Angular velocities in the N frame")
ax3.set_ylabel("Omega [rad/sec]")
ax3.set_xlabel("Time [sec]")
ax3.legend(handles = handles)

plt.show()