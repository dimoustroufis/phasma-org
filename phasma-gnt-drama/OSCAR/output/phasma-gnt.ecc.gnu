reset
set term png
set output "phasma-gnt.ecc.png"
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%Y"
set title "PHASMA-GNT\nNominal attitude 10deg pitch\nEccentricity vs. Time"
set xlabel "Date"
set ylabel "Eccentricity\nSingly averaged (over M)"
set xrange [*:*]
set yrange [*:*]
set format y "%7.5f"
set key below
plot \
"phasma-gnt.oev" u 1:5 w l lt 01 lw 01 notitle
