Intro
=====

Libre Space Foundation is developing an in-orbit validation mission composed of 3 cubesats,
with a primary focus on spectrum monitoring and [space-based space situational awareness (SSA)](https://libre.space/2020/03/02/space-situational-awareness/).

The mission has several objectives that will be fulfilled from the same space
platform (cubesat). 

Naming
======

**Project Name**: PHASMA

**Launch Campaign Name**: PHASMA-GNT

**First class of cubesats**: PHASMA-PRIMUS


**Individual Cubesat Names**:
1. PHASMA-NYQUIST
2. PHASMA-LAMARR
3. PHASMA-DIRAC


Logo
====

<img src="https://cloud.libre.space/apps/files_sharing/publicpreview/sS5STnjfWS7yD59?file=&fileId=514695&x=2160&y=3840&a=true"
     alt="Markdown Monster icon"
     style="margin: 10px; height:200px" />



Objectives
==========

The first objective is to perform in-orbit spectrum monitoring in
specific spectrum regions (UHF and S-band). The satellite will monitor both terrestrial and
satellite transmissions. On-board DSP processing blocks will identify the spectrum utilization
across different frequency bands together with its spatial and timing variations. Using ML
techniques that have already been tested in simulated and real-world environments, the
system will also classify the transmissions in terms of modulation and bandwidth as well as
their origin (terrestrial or space). Post processing of the results on the ground can be used to
quantify spectrum utilization around the globe, identify sources of interference and spot
regulatory violations. At the same time, the received signals can be also used to perform
orbit determination and satellite identification for space originating transmissions, thus
acting as a Space Situational Awareness sensor from space. The mission will act as
technology validation for several LSF developed systems and technologies, like the
[SatNOGS-COMMS](https://libre.space/projects/satnogs-comms/), a transceiver with enhanced capabilities suitable for CubeSats and
several software based technologies developed during several ESA contracts. The mission
will act as a precursor for future services, providing vital information and identifying the
challenges and requirements of such services.
